create database Vinils_Record
go

use Vinils_Record
go

create table person_(
	id uniqueidentifier primary key not null,
	firstname varchar(80) not null,
	lastname varchar(80) not null,
	dateOfBirth date not null,
	emailAddress varchar(50) not null,
	avatar varchar(200) not null
)
go

create table user_(
	id uniqueidentifier primary key foreign key references person_(id) not null,
	phoneNumber char(10) not null,
	type int not null
)
go

create table address_(
	id uniqueidentifier primary key,
	zipcode char(5) not null,
	street varchar(30) not null,
	streetNumber varchar(30) not null,
	province varchar(30) not null,
	city varchar(30) not null,
	state varchar(30) not null,
	country varchar(30) not null
)
go

create table user_address(
	uId uniqueidentifier foreign key references user_(id) not null,
	aId uniqueidentifier foreign key references address_(id) not null,
	type int not null,
	constraint PK_user_address primary key(uId, aId)
)
go

create table company_(
	id int primary key identity(1,1) not null,
	name varchar(50) not null,
	website varchar(50) not null,
	phoneNumber char(10) not null,
	localContact varchar(100) not null
)
go

create table companyAddress_(
	id int primary key foreign key references company_(id) not null,
	zipcode char(5) not null,
	street varchar(30) not null,
	streetNumber varchar(30) not null,
	province varchar(30) not null,
	city varchar(30) not null,
	state varchar(30) not null,
	country varchar(30) not null
)
go

create table paymentMethod_(
	id uniqueidentifier primary key not null,
	name varchar(30) not null,
	cardNumber char(16) not null,
	cvv char(3) not null, 
	type int not null,
	bank varchar(30) not null
)
go

create table paymentMethod_user(
	pmId uniqueidentifier foreign key references paymentMethod_(id) not null,
	uId uniqueidentifier foreign key references user_(id) not null,
	constraint PK_paymentMethod_user primary key(pmId, uId)
)
go

create table genre_(
	id int primary key identity(1,1) not null,
	name varchar(50) not null,
	description text null
)
go

create table singer_(
	id int primary key identity(1,1) not null,
	name varchar(50) not null
)
go

create table product_(
	id uniqueidentifier primary key not null,
	name varchar(30) not null,
	description text not null,
	genre_ int foreign key references genre_(id) not null,
	singer_ int foreign key references singer_(id) not null,
	album varchar(30) null,
	year char(4) null,
	status int not null,
	regularPrice decimal(5,2) not null,
	price decimal(5,2) not null,
	stock int not null
)
go

create table wishList_(
	id uniqueidentifier primary key not null,
	name varchar(30) not null,
	user_ uniqueidentifier foreign key references user_(id) not null
)
go

create table wishList_product(
	wlId uniqueidentifier foreign key references wishList_(id) not null,
	pId uniqueidentifier foreign key references product_(id) not null,
	constraint PK_wishList_product primary key(wlId, pId)
)
go

create table image_(
	id uniqueidentifier primary key not null,
	url varchar(100) not null
)
go

create table image_product(
	iId uniqueidentifier foreign key references image_(id) not null,
	pId uniqueidentifier foreign key references product_(id) not null,
	constraint PK_image_product primary key(iId, pId)
)
go

create table order_ (
	id uniqueidentifier primary key not null,
	user_ uniqueidentifier foreign key references user_(id) not null,
	status int not null,
	datetime datetime not null,
	paymentMethod uniqueidentifier foreign key references paymentMethod_(id) not null,
	total decimal(5,2) not null
)
go

create table productList_(
	oId uniqueidentifier foreign key references order_(id) not null,
	pId uniqueidentifier foreign key references product_(id) not null,
	lot int not null,
	amount decimal(5,2) not null,
	constraint PK_order_product primary key(oId, pId)
)
go

create table shipment_(
	id uniqueidentifier primary key not null,
	company_ int foreign key references company_(id) not null,
	order_ uniqueidentifier foreign key references order_(id) not null,
	trackingNumber varchar(100) not null,
	timePrepared datetime not null,
	timePickedUp datetime not null,
	timeDelivered datetime not null
)
go